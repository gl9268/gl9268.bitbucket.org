
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var trenutnateza;
var trenutnavisina;
var teze = [];
var visine = [];
var datumi = [];
var trenutniBMI;
var graf;
var datum;
var chart;

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 
 
 
 var imena = ["Andrej", "Anton", "Jože"];
 var priimki = ["Novak", "Horvat", "Zupančič"];
 var rojstva = ["1970-05-12T14:14", "1980-02-12T16:22", "1951-10-23T08:11"];
 
 var visine1 = [
 	[150, 155, 160],
 	[170, 176, 185],
 	[180, 185, 190]
 ];
 
 
  var teze1 = [
 	[45, 50, 52],
 	[70, 80, 90],
 	[80, 90, 100]
 ];
 
 var dinh = ["2015-10-11T11:20Z", "2016-10-11T11:20Z", "2017-10-11T11:20Z"];
 
   var temps = [
 	[40, 40, 40],
 	[39, 39, 39],
 	[37, 37, 37]
 ];
 
 
 var sistlak = [100, 100, 100];
 var distlak = [100, 100, 100];
 var nkri = [90, 90, 90];
 
 
 
function generirajPodatke(stPacienta) {
		sessionId = getSessionId();
 var ehrId = "";



		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		         ehrId = data.ehrId;
		        var partyData = {
		            firstNames: imena[stPacienta-1],
		            lastNames: priimki[stPacienta-1],
		            dateOfBirth: rojstva[stPacienta-1],
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                
		                
		                
		      for (var i = 0; i <3 ; i ++){          		                
	$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": dinh[stPacienta-1],
		    "vital_signs/height_length/any_event/body_height_length": visine1[stPacienta-1][i],
		    "vital_signs/body_weight/any_event/body_weight": teze1[stPacienta-1][i],
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": temps[stPacienta-1][i],
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistlak[stPacienta-1],
		    "vital_signs/blood_pressure/any_event/diastolic": distlak[stPacienta-1],
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nkri[stPacienta-1]
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: "Ime Priimek"
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		      
		    },
		    error: function(err) {
		
		    }
		});
		                
		      }
		      
		                   console.log(ehrId);
		                   
		 	$("#generirajEHR").append("<span class='obvestilo " +
								"label label-success fade-in'>Uspešno generiran EHR ID '" +
								ehrId + "'.</span><br>");
		                   
		                   
		                   
		                }
		            },
		            error: function(err) {
		            console.log(err);
		            }
		        });
		    }
		});
		
   return ehrId;
 
}


window.onload = function() {
	document.getElementById("generiraj").onclick = function() { 

		
			
			$("#generirajEHR").html("<span class='obvestilo label " +
      "label-warning fade-in'></span>");
		
		  generirajPodatke(1);
		 generirajPodatke(2);
		  generirajPodatke(3);
	

		
		
	}
	
};

function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}






/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();
	
	

	
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var tip = $("#preberiTipZaVitalneZnake").val();

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
		  
		  
		  	$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
		  
		  
				if (tip == "telesna temperatura") {
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].temperature +
                          " " + res[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} 
				
				
				
				
				else if (tip == "telesna teža") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
							//	trenutnateza = res[0].weight;
								//					teze = []; 
								 //for (var i = 0 ; i < res.length; i++){
								//	 teze.push(res[i].weight);
									
							//	 }
							
							

								
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].weight + " " 	+
                          res[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
								
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} 
				
				
				
				
				
				
				
				
				else if (tip == "telesna višina") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "height",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						//		trenutnavisina = res[0].height;	
						//	visine = [];
							//datumi = [];
								// for (var i = 0 ; i < res.length; i++){
							
						//		visine.push (res[i].height);
							//		 datumi.push(res[i].time);
								//	console.log(res[i].height);
								 //}
							
								
								
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna visina</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].height + " " 	+
                          res[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
								trenutnavisina = -1; 
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} 
				
				
				

				
				
				else if (tip == "telesna temperatura AQL") {
					var AQL =
						"select " +
    						"t/data[at0002]/events[at0003]/time/value as cas, " +
    						"t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude as temperatura_vrednost, " +
    						"t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/units as temperatura_enota " +
						"from EHR e[e/ehr_id/value='" + ehrId + "'] " +
						"contains OBSERVATION t[openEHR-EHR-OBSERVATION.body_temperature.v1] " +
						"where t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude<35 " +
						"order by t/data[at0002]/events[at0003]/time/value desc " +
						"limit 10";
					$.ajax({
					    url: baseUrl + "/query?" + $.param({"aql": AQL}),
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	var results = "<table class='table table-striped table-hover'>" +
                  "<tr><th>Datum in ura</th><th class='text-right'>" +
                  "Telesna temperatura</th></tr>";
					    	if (res) {
					    		var rows = res.resultSet;
						        for (var i in rows) {
						            results += "<tr><td>" + rows[i].cas +
                          "</td><td class='text-right'>" +
                          rows[i].temperatura_vrednost + " " 	+
                          rows[i].temperatura_enota + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
								
								
								
								
								
								
								
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}

					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				}
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}


$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

	
	$('#preberiEhrIdZaBMI').change(function() {
		$("#BMIBolnika").html("");
		$("#BMIF").html("");
		$("#EHRzaBMI").val($(this).val());
	graf = [];
	visine = [];
	datumi = [];
	teze = [];
	datum = [];
	chart.clearChart();
	});
	
	
	
	
});



function izracunajBMI() {
	sessionId = getSessionId();

	var ehrId = $("#EHRzaBMI").val();

	if (!ehrId || ehrId.trim().length == 0 ) {
		$("#preberiEhrIdZaBMI").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
			
		  

					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
							trenutnateza = res[0].weight;
							teze = [];
								 for (var i = 0 ; i < res.length; i++){
								 teze.push(res[i].weight);	
								 }		



						$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "height",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
								trenutnavisina = res[0].height;	
								visine = [];
								datumi = [];
							 for (var i = 0 ; i < res.length; i++){				
									visine.push(res[i].height);
									 datumi.push(res[i].time);
								 }		
								 
								 var tekst = ""; 
								 
							
								 
						trenutniBMI = Math.round(trenutnateza / (trenutnavisina/100 * trenutnavisina/100) * 100) / 100 ;
						
								 if (trenutniBMI < 18.5) tekst = " .Pacient tehta premalo. Priporočamo, da se posvetuje z zdravnikom.";
								 else if (trenutniBMI < 24.5 && trenutniBMI > 18.5) tekst = ". Teža pacienta je ustrezna.";
								 else if (trenutniBMI < 30 && trenutniBMI > 25) tekst = ". Pacient tehta nekoliko preveč. Priporočamo, da se posvetuje z zdravnikom.";
								  else if (trenutniBMI > 30 ) tekst = ". Pacient tehta preveč. Zelo priporočamo, da se posvetuje z zdravnikom.";
		$("#neki").html(" <p>Pacientov trenutni BMI je " + trenutniBMI + tekst + "</p>");
		
				console.log(teze);
				console.log(visine);
				console.log(datumi);
			
			 graf = []; 
			 graf [0] = ['Datum', 'BMI'];
			 datum = [];
			for (var i = 1; i < visine.length; i++ ) {
			graf[visine.length - i] = ( [datumi[i-1], (Math.round(         teze[i-1]/ (visine[i-1]/100 * visine[i-1]/100)*100)/100 )]);
				
				}
			console.log(graf);

				narisigraf ();
				
				
					    	} else {
								trenutnavisina = -1; 
					    		$("#BMIBolnika").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#BMIBolnika").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
			
								 
					    	} else {
								
					    		$("#BMIBolnika").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#BMIBolnika").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				
				
				

	 

				
				
	    	}
			
			
		});
	}
	
	
		
		
	
}	



	function narisigraf (){
		
		
	   google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable(graf);

        var options = {
          title: '',
          legend: { position: 'bottom' }
        };

         chart = new google.visualization.LineChart(document.getElementById('BMIF'));

        chart.draw(data, options);
      }

		
	}
	
	
	





	var map;
	var infowindow; 
	var request; 
	var service;
	var markers = [];
	
	function zacni (){
	
		var center = new google.maps.LatLng(46.056946, 14.505751);
		map = new google.maps.Map (document.getElementById('map'), { 
		center: center,
		zoom: 9
	});
	
		request = {
		location: center,
		radius: 1000, // v metrih
		types: ['hospital']
		};
	
	infowindow = new google.maps.InfoWindow();
	
	service = new google.maps.places.PlacesService(map);
	service.nearbySearch (request, callback);
	
	
	 google.maps.event.addListener (map, 'rightclick', function (event){
		map.setCenter (event.latLng);
		clearResults(markers);
		
		var request = {
		location: event.latLng,
		radius: 1000,
		types: ['hospital']
	}; 
	
	service.nearbySearch (request, callback);
		}) 
		
	} 
	
	function callback (results, status){
	if (status == google.maps.places.PlacesServiceStatus.OK){
		for (var i = 0; i < results.length; i++){
			markers.push(createMarker(results[i]));
			//console.log(results);
			
		//	console.log(results[0].vicinity); 
		//	console.log(results[0].name); 
			}
		}
	}
	
	
	function createMarker (place){
	var placeLoc = place.geometry.location;
	var marker = new google.maps.Marker ({
		map: map, 
		position: place.geometry.location
	});
	
	google.maps.event.addListener(marker, 'click', function (){
		infowindow.setContent(place.name);
		infowindow.open(map, this);
	});
	return marker;
	}
	
	function clearResults (markers){
		for (var i= 0 ; i < markers.length; i++){
		 markers [i].setMap(null);
		}
		markers = [];
	} 
	
	
